
***

## PEF

***

## Projeto: Votação

>	Ao final do semestre deverá ser desenvolvido um projeto final. 
> 	Este projeto deverá fazer uso de tecnologia .NET. 
> 	Precisará fazer uso de aplicativos: 
>   - Desktop (WPF), 
>	- Mobile (com Windows Phoje) e 
>	- Web (com ASP.NET MVC e Entity Framework). 
> 	Na apresentação do projeto, os alunos serão indagados e o não atendimento à indagação resulta em não aprovação. O projeto final é eliminatório, ou seja, se não apresentar ou não for aprovado, o aluno não obtém aprovação na disciplina.
